# Nuit de l’Info 2022

Equipe LRV

Valentin ROMANET

Yohann LOISEAU

Colin PILET

## Description du projet

Nous avons créé un site internet qui donne des informations sur le thème demandé. Nous avons également ajouté un quiz avec des questions pour rendre tout cela plus ludique.
Durant cette nuit, nous avons choisis de réaliser 2 défis : Easter Egg et Movai Code.

## Movai Code

Pour accéder à la partie « Movai Code » du site, il faut cliquer sur le lien caché dans la page d’accueil. Il se trouve à droite de « All right reserved ». 

## Easter egg

Pour la partie Easter Egg, voici la liste des easter eggs que nous avons cachés :
- La page « Mauvais code » du site cliquant sur le lien caché à droite de « All right reserved » sur la page d’accueil
- Le Trooper caché sur la page « Mauvai Code du site »
- Le cri de Wilhelm lorsque l’on clique sur le Trooper
- « All bases are belong to us » en texte invisible à la fin du paragraphe « Nuit de l’Info » sur la page d’accueil (sélectionner le texte pour le voir)
- Easter egg bonus en cliquant sur le logo de la page d’accueil
- Pour les musiques, il se peut qu'il faille autoriser le son sur la page

## Lien du site
lrv2.alwaysdata.net
