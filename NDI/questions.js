// creating an array and passing the number, questions, options, and answers
let questions = [
    {
    numb: 1,
    question: "Qu'est ce que le VIH ?",
    answer: "Virus de l'Immonudéficience Humaine",
    options: [
      "Virus Idrogénique Humain",
      "Virus de l'Immonudéficience Humaine",
      "Vaccin contre l'Infection Hormonale",
      "Vaccin contre l'Insuffisance Hépatique"
    ]
  },
    {
    numb: 2,
    question: "Qu'est ce qu'une IST ?",
    answer: "Infection Sexuellement Transmissible",
    options: [
      "Indépendance au Sucre Totale",
      "Infection Super Transmissible",
      "Infection du Sang Transmissible",
      "Infection Sexuellement Transmissible"
    ]
  },
    {
    numb: 3,
    question: "Comment éviter d'attraper une IST",
    answer: "Se protéger",
    options: [
      "Fuir",
      "Ne rien faire",
      "Se protéger",
      "Le faire sans protection"
    ]
  },
    {
    numb: 4,
    question: "Quels moyens de protections sont efficaces ?",
    answer: "Les préservatifs",
    options: [
      "Les préservatifs",
      "La pilule du lendemain",
      "Aucune",
      "Les deux premières"
    ]
  },
    {
    numb: 5,
    question: "Est ce que toutes les IST se soignent ?",
    answer: "Oui très facilement",
    options: [
      "Oui mais avec quelques difficultés",
      "Pas du tout",
      "Avec des traitements lourds",
      "Oui très facilement"
    ]
  },
  // you can uncomment the below codes and make duplicate as more as you want to add question
  // but remember you need to give the numb value serialize like 1,2,3,5,6,7,8,9.....
  //   {
  //   numb: 6,
  //   question: "Your Question is Here",
  //   answer: "Correct answer of the question is here",
  //   options: [
  //     "Option 1",
  //     "option 2",
  //     "option 3",
  //     "option 4"
  //   ]
  // },
];